import os
from os.path import abspath, dirname, isfile
import markdown
import indexer
import difflib
import patch
from flask import Flask
from flask import request
from flask import send_from_directory, render_template
from werkzeug.utils import secure_filename
from libs.markdown.video import VideoExtension
from libs.markdown.imagestyle import ImageStyleExtension

CWD = abspath(dirname(__file__))
STATIC_IMAGE_PATH = CWD + '/src/images/'
STATIC_VIDEO_PATH = CWD + '/src/videos/'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif', 'svg'}

app = Flask(__name__)
app.debug = True
app.config['STATIC_IMAGE_PATH'] = STATIC_IMAGE_PATH
app.config['STATIC_VIDEO_PATH'] = STATIC_VIDEO_PATH


# ROUTES ----------------------------------------------------------------------

@app.route('/')
@app.route('/articles/')
@app.route('/articles/<category>')
@app.route('/articles/<category>/<article>')
def articles(category='_Home', article='index'):
    path_to_articles = get_path_to_articles()
    menu = create_menu(path_to_articles)

    title = get_article_title(category, article)
    title = prettify_article_title(title)

    template = 'article.html'
    return render_template(template, menu=menu, content='', toc='', title=title)


@app.route('/ajax/')
@app.route('/ajax/articles/')
@app.route('/ajax/articles/<category>')
@app.route('/ajax/articles/<category>/<article>')
def ajax_articles(category='_Home', article='index'):
    path_to_articles = get_path_to_articles()

    text = get_article_text(path_to_articles, category, article)
    if text == '':
        return render_template('404_ajax.html', menu={})

    title = get_article_title(category, article)
    title = prettify_article_title(title)
    
    md = markdown.Markdown(extensions=['codehilite', 'toc', 'nl2br', 'tables', 'footnotes', VideoExtension(), ImageStyleExtension()])# 'scruffy(output_dir=' + path_to_articles + 'images/)'])
    html = md.convert(text)
    toc = md.toc

    template = 'article_ajax.html'
    return render_template(template, menu={}, content=html, toc=toc, title=title)


@app.route('/search', methods=['POST'])
def search(ajax=False):
    path_to_articles = get_path_to_articles()
    menu = create_menu(path_to_articles)
    search_term = preprocess_search_term(request.form['term'])

    title = 'Search'
    index = indexer.Indexer()
    results = index.search_file(search_term)
    results = results_to_markdown(results)

    template = 'search_results.html' if not ajax else 'search_results_ajax.html'
    return render_template(template, menu=menu, results=results, title=title)


@app.route('/source/<category>/<article>')
def source(category='_Home', article='index'):
    path_to_articles = get_path_to_articles()
    filename = category + '/' + article + '.md'
    return send_from_directory(path_to_articles, filename)


@app.route('/save/<category>/<article>', methods=['POST'])
def save(category='_Home', article='index'):
    confirm = request.values.get('confirm', None)
    if confirm:
        confirmed_diff = request.values.get('confirmed-diff', None)
        apply_patch(confirmed_diff)
        return ajax_articles(category, article)
    else:
        new_content = request.values.get('content', None)
        path_to_articles = get_path_to_articles()
        filename = category + '/' + article + '.md'
        return make_patch(path_to_articles, filename, new_content)


@app.route('/upload', methods=['POST'])
def upload_image():
    if request.method == 'POST':
        # Check if the post request has the file part
        if 'file' not in request.files:
            return '{ "success": false, "error": "File missing in request" }'

        file = request.files['file']
        if file.filename == '':
            return '{ "success": false, "error": "Filename missing" }'

        filename = secure_filename(file.filename)
        file_path = os.path.join(app.config['STATIC_IMAGE_PATH'], filename)
        if os.path.isfile(file_path):
            return '{ "success": false, "error": "File with the same name already exists" }'
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['STATIC_IMAGE_PATH'], filename))
            return '{ "success": true, "path": "![%s](/images/%s)" }' % (filename, filename)
        else:
            return '{ "success": false, "error": "File type is not supported" }'
    return '{ "success": false, "error": "General upload error" }'


# Custom static data
@app.route('/images/<path:filename>')
def images(filename):
    return send_from_directory(app.config['STATIC_IMAGE_PATH'], filename)

@app.route('/videos/<path:filename>')
def videos(filename):
    return send_from_directory(app.config['STATIC_VIDEO_PATH'], filename)

@app.route('/files/<path:filename>')
def files(filename):
    cwd = abspath(dirname(__file__))
    path = cwd + '/src/files/'
    return send_from_directory(path, filename)


# UTILS ----------------------------------------------------------------------=

# Methods usable in Jinja2 templates
@app.context_processor
def template_methods():
    return dict(titleToStyleName=title_to_style_name)


def path_to_style_name(path):
    title = path.split('/')[1]
    return title_to_style_name(title)


def title_to_style_name(title):
    return title.lower().replace('_', '').replace(' ', '').replace('+', 'p')


def path_to_title_bar(path, divider):
    path = path.replace('_', ' ')
    parts = path.split('/')
    parts.pop(0)
    return divider.join(parts)


def prettify_article_title(path):
    style_name = path_to_style_name(path)
    title = path_to_title_bar(path, '<i class="divider svg-toggle"></i>')
    return '<span class="title"><i class="svg-' + style_name + '"></i>' + title + '</span>'


def get_path_to_articles():
    cwd = abspath(dirname(__file__))
    return cwd + '/src/'


def get_article_text(path, category, article):
    text = ''
    filename = path + category + '/' + article + '.md'
    if isfile(filename):
        text = open(filename, 'r', encoding='utf-8').read()
    return text


def get_article_title(category, article):
    return ('/' + category + '/' + article).replace('_', ' ')


def results_to_markdown(results):
    md = markdown.Markdown(extensions=['codehilite', 'nl2br'])
    mardownized_results = []
    for result in results:
        res = {
            'title': result['title'],
            'path':  result['path'],
            'text': md.convert(result['content'])
        }
        mardownized_results.append(res)

    return mardownized_results

def preprocess_search_term(term, min_length=3):
    '''
    Wrapping shorter terms (<3 characters) in quotes to treat them as exact matches
     to avoid exception in whoosh
    '''
    words = term.split()
    processed_words = [
        f'"{word}"' if len(word) < min_length else word for word in words
    ]
    return " ".join(processed_words)

# FILE UTILS ------------------------------------------------------------------

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


def apply_patch(confirmed_diff):
    cwd = abspath(dirname(__file__))
    p = patch.fromstring(bytes(confirmed_diff, 'utf-8'))
    success = p.apply(root=cwd + '/src')
    print('Patch applies return: ' + str(success))


def make_patch(path_to_articles, filename, new_content):
    with open(path_to_articles + '/' + filename, 'r', encoding='utf-8') as f:
        old_content = f.read()
    diff = ''
    for line in difflib.unified_diff(old_content.splitlines(1), new_content.splitlines(1), fromfile=filename, tofile=filename):
        diff += line
        diff += '\n' if line[-1] != '\n' else ''
    return diff


# MENU -----------------------------------------------------------------------

def create_menu(path):
    menu = {}
    create_menu_recursive(menu, path)
    return menu


def create_menu_recursive(menu, path):
    """ Creates a map with  directory structure """
    dirs = os.listdir(path)
    for file in dirs:
        basename = os.path.splitext(file)[0]
        if basename.startswith('.') or basename.startswith('image') or basename.startswith('files') or basename.startswith('videos'):
            continue 
        menu[basename] = {}
        if os.path.isdir(path + file):
            create_menu_recursive(menu[file], path + file)
            menu[file] = item_convert(menu[file])


def item_convert(menuitem):
    if len(menuitem) == 1:
        return menuitem.popitem()[0]
    else:
        return menuitem

if __name__ == '__main__':
    CSRF_ENABLED = True
    app.debug = True
    app.run()
