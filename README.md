Markdown Knowledge Base
=======================
Features:

- Uses Markdown for documents, because WYSIWYG sucks
- Menu is generated directly from directory structure
- Online Markdown editor - CodeMirror
- Drag&Drop images, with automatic link creation
- Autoscroll Table of Contents
- Design borrowed from [devdocs.io](https://devdocs.io)
- Responsive design
- Pygments syntax highlighter
- Whoosh indexer for searching in notes
- MathJax included
- UML diagram generator
- Documents can be versioned
- Written in Python

![Example](https://bitbucket.org/lukash/markdown-note-site/raw/master/src/images/markdown-note-site.png)

Install
=======
Install system packages:

- python-virtualenv
- plotutils
- graphviz

Execute as normal user:

    virtualenv virtualenv/
    source virtualenv/bin/activate

    pip install -r requirements.txt

Start local instance:

    python ./knowledges.py

Indexing
========
To enable searching in documents, we need to run indexer, after every change.

    python ./indexer.py

Schedule reindexing every night:

    30 2 * * * cd /srv/http/saix && virtualenv/bin/python indexer.py

Dependencies
============

- flask
- markdown
- pygments

Optional:

- whoosh (For search)
- scruffy (For diagrams)
- mathjax??

Apache
======
Deployment to Apache:

Set virtualenv path in  `knowledges.wsgi`

    activate_this = '<path_to_virtualenv>/bin/activate_this.py'

Add new virtualhost to Apache configuration. Set links and paths in configuration properly.

    <VirtualHost *:80>
        ServerName www.domain.com
        ServerAlias domain.com
        DocumentRoot /srv/http/knowledges/
    
        WSGIDaemonProcess knowledges user=saix group=saix hthreads=1 python-path=/srv/http/knowledges/
        WSGIScriptAlias / /srv/http/knowledges/knowledges.wsgi
        Alias /favicon.ico /srv/http/knowledges/favicon.ico
    
        <Directory /srv/http/knowledges/>
            Options ExecCGI Indexes
            AddHandler cgi-script .cgi
            AddHandler wsgi-script .wsgi
    
            AuthType Basic
            AuthName "Restricted Files"
            AuthBasicProvider file
            AuthUserFile /etc/apache2/httppasswd
            Require user saix
    
            WSGIProcessGroup knowledges
            WSGIApplicationGroup %{GLOBAL}
            Order allow,deny
            Allow from all
        </Directory>
    </VirtualHost>


SCRUFFY - class diagram generator
=================================
To enable scruffy extension:

    install font Purisa
    graphviz
    sudo apt-get install python-dev

Install scruffy library to virtual environment

    cd libs/scruffy
    python setup.py install

Copy scruffy extension to markdown

    cp libs/markdown/scruffy.py <virtualenvpath>/site-packages/markdown/extensions/scruffy.py

Cron job for automatic update
=============================
Add checking of git repository to cron (every 5 minutes)

    */5 * * * * cd /srv/http/knowledges/src && /usr/bin/git pull origin master 2> /dev/null > /dev/null 

Javascript development
======================
After some changes in javascript files run:

    npm install
    npm run build
