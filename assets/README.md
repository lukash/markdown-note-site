Adding New Images
=================
Image should be minimized using Inkscapes simplify (Path > Simplify) functionality.
Then save it as "Optimized SVG" with precision "5" significant digits, removed all
unnecessary metadata and comments and shorten IDs.

Build Image Sprite
==================
Execute in current directory:

    npm install
    npm run svg-sprite

    cp out/svg/sprite.svg ../static/svg/
    cp out/css/sprite.css ../static/css/
