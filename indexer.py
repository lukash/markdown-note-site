#!/usr/bin/env python
import os, sys
import re
import json
import unicodedata
import pathlib
from os.path import abspath, dirname
from whoosh.index import create_in, open_dir
from whoosh.qparser import MultifieldParser, FuzzyTermPlugin
from whoosh.fields import *
from whoosh.analysis import CharsetFilter, StemmingAnalyzer
from whoosh.support.charset import accent_map

cwd = abspath(dirname(__file__))
DEBUG = False
INDEX_DIR = str(cwd) + '/index'

class TextProcessor(object):
    currentTitle = ''
    breadcrumbs = []
    currentElement = {'title': '', 'breadcrumbs': '', 'url': '', 'content': ''}
    elementTree = []
    blocks = {}

    def parse(self, file_path: str, text: str):
        file_path = file_path.replace('_', ' ').replace('-', ' ')
        self.elementTree = []
        self.blocks = {}
        self.breadcrumbs = [file_path]
        text = Utils.trim_lines(text)
        self.blocks = text.split('\n\n')
        for block in self.blocks:
            self._header_processor(file_path, block)
            self._paragraph_processor(block)

        return self.elementTree

    def _header_processor(self, file_path: str, block: str):
        block = block.strip()
        lines = block.split('\n')
        if len(lines) < 2:
            return
        if re.match(r'={2}', lines[1]):
            self.currentTitle = lines[0]
            self.breadcrumbs = [file_path, self.currentTitle]
        elif re.match(r'-{2}', lines[1]):
            self.currentTitle = lines[0]
            self.breadcrumbs = [file_path, self.breadcrumbs[1] if len(self.breadcrumbs) > 1 else '', self.currentTitle]
        elif re.match(r'### (.*)', lines[0]):
            self.currentTitle = lines[0].lstrip('#').strip()
            self.breadcrumbs = [file_path, self.breadcrumbs[1] if len(self.breadcrumbs) > 1 else '', self.breadcrumbs[2] if len(self.breadcrumbs) > 2 else '', self.currentTitle]
        else:
            return
        self.currentElement = {
            'title': self.currentTitle,
            'breadcrumbs': self.breadcrumbs,
            'url': '#' + Utils.slugify(lines[0], '-'),
            'content': ''}
        self.elementTree.insert(0, self.currentElement)

    def _paragraph_processor(self, block: str):
        self.currentElement['content'] = block


class Utils(object):

    @staticmethod
    def trim_lines(text):
        lines = text.split('\n')
        lines_trimmed = []
        for line in lines:
            lines_trimmed.append(line.rstrip())
        return '\n'.join(lines_trimmed)

    @staticmethod
    def slugify(uvalue, separator):
        #print('Slugify: ' + value)
        """ Slugify a string, to make it URL friendly. """
        uvalue = unicodedata.normalize('NFKD', uvalue)
        value = re.sub('[^\w\s-]', '', uvalue).strip().lower()
        return re.sub('[%s\s]+' % separator, separator, value)

    @staticmethod
    def walk_dir(path):
        listed_files = []
        Utils.walk_dir_recurse(listed_files, path, '')
        for f in listed_files:
            yield f

    @staticmethod
    def walk_dir_recurse(listed_files, base_path, relative_path):
        """ Creates index from markdown files in given
            directory structure """
        dirs = os.listdir(base_path + relative_path)
        for file in dirs:
            basename = os.path.splitext(file)[0]
            if basename.startswith('.') or basename.startswith('image') or basename.startswith('files') or basename.startswith('videos'):
                continue
            if os.path.isdir(base_path + relative_path + file):
                Utils.walk_dir_recurse(listed_files, base_path, relative_path + file + '/')
            else:
                listed_files.append(relative_path + file)


class Indexer:
    schema = None

    def __init__(self):
        my_analyzer = StemmingAnalyzer() | CharsetFilter(accent_map)
        self.schema = Schema(title=TEXT(stored=True, field_boost=2.0), breadcrumbs=TEXT(stored=True, field_boost=1.5), path=TEXT(stored=True), content=TEXT(stored=True, analyzer=my_analyzer))

    def index_path(self, basePath, url):
        if not os.path.exists(INDEX_DIR):
            os.mkdir(INDEX_DIR)
        ix = create_in(INDEX_DIR, self.schema)
        writer = ix.writer()
        writer.start_group()
        for relative_path in Utils.walk_dir(basePath):
            self._index_file_title(writer, url, basePath, relative_path)
            self._index_file(writer, url, basePath, relative_path)
        writer.end_group()
        writer.commit()

    def _index_file_title(self, writer, url, base_path, relative_path):
        content = ''
        with open(base_path + relative_path, 'r', encoding='utf-8', errors='ignore') as f:
            for i, line in enumerate(f):
                if i > 5:
                    break
                content += line
        relative_path = os.path.splitext(relative_path)[0]
        filename = os.path.basename(relative_path)
        writer.add_document(title=filename, breadcrumbs=filename, path=url+relative_path, content=content, _title_boost=20.0)

    def _index_file(self, writer, url, base_path, relative_path):
        print('Indexing file: ' + relative_path)
        with open(base_path + relative_path, 'r', encoding='utf-8') as f:
            content = f.read()

        relative_path = os.path.splitext(relative_path)[0]
        text_processor = TextProcessor()
        blocks = text_processor.parse(relative_path, content)
        for block in blocks:
            if DEBUG:
                print(json.dumps(block))
            title = block['title']
            breadcrumbs = '/'.join(block['breadcrumbs'])
            path = url + relative_path + block['url']

            paragraph = block['content']
            writer.add_document(title=title, breadcrumbs=breadcrumbs, path=path, content=paragraph, _title_boost=20.0, _breadcrumbs_boost=20.0)

    def search_file(self, pattern):
        ret = list()
        ix = open_dir(INDEX_DIR)
        with ix.searcher() as searcher:
            # add pattern~1 fuzzy
            pattern = ' '.join([p + '~1/3' for p in pattern.split()])
            parser = MultifieldParser(['title', 'breadcrumbs', 'content'], ix.schema)
            parser.add_plugin(FuzzyTermPlugin())
            query = parser.parse(pattern)
            results = searcher.search(query, limit=30)
            printed_doc = list()
            for result in results:
                if result['path'] in printed_doc:
                    continue
                printed_doc.append(result['title'])
                ret.append(result)

        return ret


if __name__ == '__main__':
    path = cwd + '/src/'
    url = 'articles/'

    indexer = Indexer()
    indexer.index_path(path, url)
    # Example search: results = indexer.search_file(u'code');
    print('[DONE]')
