var menuExpanded = {};
var documentDiffToConfirm = undefined;
var editor = undefined;

var DEFAULT_RELATIVE_URL = "/articles/_Home/index";

$(function () {
    $(".submenu").hide();
    $("#button-toc-tag").click(function() {
        toggleTableOfContents();
    });
    $("#button-menu").click(function() {
        toggleMenu();
    });
    $("#button-source-download").click(function() {
        var encodedUrl = document.location.href;
        var relativeUrl = toRelativeUrl(encodedUrl);
        relativeUrl = relativeUrl.replace("articles", "source");
        window.open(relativeUrl, "Download");
    });
    $("#button-edit").click(function() {
        var encodedUrl = document.location.href;
        var relativeUrl = toRelativeUrl(encodedUrl);
        relativeUrl = relativeUrl.replace("articles", "source");

        $.get({
          url: relativeUrl,
          cache: false
        }).then(function(documentText){
          toggleEditor(documentText);
        });
    });
    $("#button-cancel").click(function() {
        toggleEditor();
    });
    $("#button-dialog-cancel").click(function() {
        documentDiffToConfirm = undefined;
        hideDiffDialog();
    });
    $("#button-save").click(function() {
        var encodedUrl = document.location.href;
        var relativeUrl = toRelativeUrl(encodedUrl);
        var saveUrl = relativeUrl.replace("articles", "save");

        editor.save();
        var editTextarea = document.getElementById("editor-textarea")

        $.post(saveUrl, { "content": editTextarea.value }, function(documentDiff) {
            documentDiffToConfirm = documentDiff;
            showDiffDialog(documentDiff)
            loadArticleByUrl(relativeUrl);
        });
    });
    $("#button-dialog-confirm").click(function() {
        if (documentDiffToConfirm) {
            var encodedUrl = document.location.href;
            var relativeUrl = toRelativeUrl(encodedUrl);
            var saveUrl = relativeUrl.replace("articles", "save");

            var editTextarea = document.getElementById("editor-textarea")
            $.post(saveUrl, { "confirmed-diff": documentDiffToConfirm, "confirm": true }, function(documentDiff) {
                documentDiffToConfirm = undefined;
                hideDiffDialog();
                toggleEditor();
                loadArticleByUrl(relativeUrl);
            });
        }
    });

    $(".menuitem_group").click(function() {
        var classes = $(this).attr("class");
        toggleMenuGroup(classes.split(" ")[1]);
        return false;
    });
    $(".menuitem").click(function() {
        var url = $(this).attr("href");
        encodedUrl = encodeUrl(url);
        history.pushState({}, "", encodedUrl);
        loadArticleByUrl(encodedUrl);
        return false;
    });
    selectMenuItemByCurrentLocation();
    loadArticleContent();
});

function loadArticleContent() {
    var encodedUrl = document.location.href;
    var relativeUrl = toRelativeUrl(encodedUrl);
    loadArticleByUrl(relativeUrl);
}

function loadArticleByUrl(encodedUrl) {
    var spinnerTimeout = setTimeout(function() {
        $("#content").html('');
        $("#article-spinner").show();
    }, 200);

    $("#content").load("/ajax" + encodedUrl, function() {
        clearTimeout(spinnerTimeout);
        var decodedUrl = decodeUrl(encodedUrl);
        $("#article-spinner").hide();
        // If url contains hash (e.g. #pointers), do not scroll to top
        if (!window.location.hash) {
            $("#content").scrollTop(0);
        } else {
            var positionToScrollTo = $(window.location.hash).position().top;
            $("#content").scrollTop(positionToScrollTo);
        }

        updateDocumentTitle(decodedUrl);
        selectMenuItemByUrl(decodedUrl);
        $("#path").html(formatTitle(decodedUrl));

        initTableOfContent();
        initScrollSpyTableOfContent();
        initMathJax();
    });
}

// Utils ----------------------------------------------------------------------

function loadJS(url, onScriptReadyCallback) {
    var scriptTag = document.createElement("script");
    scriptTag.type = "application/javascript";
    scriptTag.src = url;
    scriptTag.onload = scriptTag.onreadystatechange = onScriptReadyCallback;
    document.body.appendChild(scriptTag);
}

// History --------------------------------------------------------------------

window.onpopstate = function(event) {
    var encodedUrl = document.location.href;
    encodedUrl = toRelativeUrl(encodedUrl);
    loadArticleByUrl(encodedUrl);
};

// Togle blocks ---------------------------------------------------------------

function loadAndToggleEditor(documentText) {
    loadJS("/static/js/codemirror.min.js", function() {
        toggleEditor(documentText);
    });
}

function toggleEditor(documentText) {
    if (typeof CodeMirror === "undefined") {
        loadAndToggleEditor(documentText);
        return;
    }
    var editTextarea = document.getElementById("editor-textarea");
    if (editor) {
        $("body").removeClass("show-editor");
        $("body").addClass("hide-editor");
        editor.save();
        editor.toTextArea();
        editor = undefined;
        editTextarea.value = documentText;
        window.onbeforeunload = null;
    } else {
        window.onbeforeunload = function(e) {
            return 'Did you save the changes?';
        };
        var articleScrollPosition = $("#content").scrollTop();
        $("body").removeClass("hide-editor");
        $("body").addClass("show-editor");

        editTextarea.value = documentText;
        editor = CodeMirror.fromTextArea(editTextarea, {
            lineNumbers: true,
            lineWrapping: true,
            mode: "text/x-markdown",
            theme: "default",
            extraKeys: {"Enter": "newlineAndIndentContinueMarkdownList"},
            showCursorWhenSelecting: true,
            dragDrop: true,
            allowDropFileTypes: ["image/png", "image/jpeg", "image/jpg", "image/gif", "image/svg+xml"]
        });
        editor.scrollTo(0, articleScrollPosition - 50);

        // On image drag&drop
        editor.on("drop", function(editor, event) {
            if (event.dataTransfer){
                event.preventDefault();
                if (event.dataTransfer.files.length) {
                    var xy = {"left": event.x, "top": event.y};
                    var newPos = editor.coordsChar(xy);
                    editor.setCursor(newPos);

                    event.stopPropagation();
                    upload(event.dataTransfer.files[0], editor);
                }
            }
        });
    }
}

function upload(fileData, editor) {
    var formData = new FormData();
    formData.append("file", fileData);
    $.ajax({
        url: "/upload",
        dataType: "text",
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        type: "post",
        success: function(ret) {
            ret = $.parseJSON(ret);
            if (ret["success"] === true) {
                editor.doc.replaceSelection(ret["path"]);
            } else {
                alert(ret["error"]);
            }
        }
    });
}

function toggleMenu() {
    if ($("#verticalmenu").css("display") === "none") {
        $("body").removeClass("hide-menu");
        $("body").addClass("show-menu");
    } else {
        $("body").removeClass("show-menu");
        $("body").addClass("hide-menu");
    }
}

function toggleTableOfContents() {
    if ($("#tocwrapper").css("display") === "none") {
        $("body").removeClass("hide-toc");
        $("body").addClass("show-toc");
    } else {
        $("body").removeClass("show-toc");
        $("body").addClass("hide-toc");
    }
}

function showDiffDialog(diff) {
    var htmlDiff = diffToHtml(diff);
    var visibleScreenHeight = $(window.top).height();
    $("#dialog .dialog-content").css("max-height", visibleScreenHeight / 2);
    $("#dialog .dialog-content").html(htmlDiff);
    $("#dialog-overlay").show();
    $("#dialog").show();
}

function hideDiffDialog(htmlDiff) {
    $("#dialog-overlay").hide();
    $("#dialog").hide();
}

function diffToHtml(diff) {
    var htmlDiff = "<p class=\"diff\">";
    var diffLines = diff.split(/\r\n|\r|\n/g);
    for (var i in diffLines) {
        var line = diffLines[i];
        var cssClass = "";
        var firstChar = line.charAt(0);
        var line = line.substring(1);
        switch(firstChar) {
            case "@": cssClass = "comment"; break;
            case "+": cssClass = "add"; break;
            case "-": cssClass = "remove"; break;
        }
        htmlDiff += "<span class=\"code " + cssClass + "\">&nbsp;" + line + "</span>";
    }

    return htmlDiff + "</p>";
}

// Title ---------------------------------------------------------------

// * Formatting methods for title bar, the same are in
//   backend, in case javascript is disabled
function formatTitle(url) {
    var styleName = pathToStyleName(url);
    var title = pathToReadableTitle(url, "<i class='divider svg-toggle'></i>");
    return "<span class='title'><i class='svg-" + styleName + "'></i>" + title + "</span>";
}

function pathToReadableTitle(path, divider) {
    path = path.replace(/_/g, " ");
    var parts = path.split("/");
    parts.shift();
    parts.shift();
    return parts.join(divider);
}

function updateDocumentTitle(decodedUrl) {
    var title = pathToReadableTitle(decodedUrl, " | ");
    document.title = "Knowledges | " + title;
}

// URL handling ---------------------------------------------------------------

function pathToStyleName(relativeUrl) {
    var path_parts = relativeUrl.split("/");
    if (path_parts.length > 2) {
        relativeUrl = path_parts[2];
        relativeUrl = relativeUrl.toLowerCase()
            .replace(/%20/g, " ")
            .replace(/\s/g, "")
            .replace(/\+/g, "p")
            .replace(/_/g, "");
    } else {
        relativeUrl = "";
    }
    return relativeUrl;
}

function encodeUrl(url) {
    return url.replace(/\s/g, "%20");
}

function decodeUrl(url) {
    return url.replace(/%20/g, " ");
}

function toRelativeUrl(absoluteUrl) {
    var parts = absoluteUrl.split("/");
    parts.shift();
    parts.shift();
    parts.shift();
    var relativeUrl = "/" + parts.join("/");
    relativeUrl = relativeUrl == "/" ? DEFAULT_RELATIVE_URL : relativeUrl
    return relativeUrl;
}

// Table of Content -----------------------------------------------------------

function initTableOfContent() {
    $(".toc a").click(function() {
        var url = $(this).attr("href");
        encodedUrl = encodeUrl(url);
        history.pushState({}, "", encodedUrl);
        var scrollTo = $("#content").scrollTop() + $(url).offset().top - 50;
        $("#content").animate({scrollTop: scrollTo}, 300);
        return false;
    });
}

function initScrollSpyTableOfContent() {
    // Cache selectors
    var lastId;
    var tableOfContent = $(".toc");
    var headerOffset = $("#header").height();
    var headingOffset = 25;
    // All list items
    var menuItems = tableOfContent.find("a");
    // Anchors corresponding to menu items
    var headerItems = menuItems.map(function(){
        var item = $($(this).attr("href"));
        if (item.length) { return {"element": item, "offset": item.offset().top - headerOffset - headingOffset } }
    });

    var lastScrollOffset = -50;

    function higlightToCByScrollPosition() {
        // Get container scroll position
        var fromTop = $("#content").scrollTop();

        // Skip, if scolled less than 50px
        if (Math.abs(fromTop - lastScrollOffset) < 50) {
            return;
        }
        lastScrollOffset = fromTop;

        // Get id of current scroll item
        var cur = headerItems.map(function(){
            if (this.offset < fromTop)
                return this.element;
        });
        // Get the id of the current element
        cur = cur[cur.length-1];
        var id = cur && cur.length ? cur[0].id : "";

        if (lastId !== id) {
            lastId = id;
            // Set/remove active class
            menuItems
             .parent().removeClass("active")
             .end().filter("[href='#"+id+"']").parent().addClass("active")
             .end().parent().parent().closest("li").addClass("active") // Select main menu if submenu is active
             .end().parent().parent().closest("li").addClass("active"); // Select main menu if subsubmenu is active
        }
    }
    higlightToCByScrollPosition();

    // Bind to scroll
    $("#content").scroll(function(){
        higlightToCByScrollPosition();
    });
}

// MathJax --------------------------------------------------------------------

function initMathJax() {
    MathJax.Hub.Config({
        displayAlign: "center"
    });
    MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
}

// Menu ------------------------------------------------------------------------

function selectMenuItemByCurrentLocation() {
    var url = document.location.href;
    url = toRelativeUrl(url);
    decodedUrl = decodeUrl(url);
    selectMenuItemByUrl(decodedUrl);
}

function selectMenuItemByUrl(decodedUrl) {
    if (decodedUrl.endsWith("/search")) {
        return;
    }
    var styleName = pathToStyleName(decodedUrl);
    decodedUrl = decodedUrl.replace(/#.*/, "");
    $(".menuitem").removeClass("selected");
    $(".menuitem[href='"+decodedUrl+"']").addClass("selected");
    if (!menuExpanded[styleName]) {
        toggleMenuGroup(styleName);
    }
}

function toggleMenuGroup(styleName) {
    var selector = ".menuitem_group";
    if (styleName && styleName.length > 0) {
        selector += "." + styleName;
    }
    var menuElem = $(selector);
    var submenu = menuElem.parent().find(".submenu");

    if (!menuExpanded[styleName]) {
        closeAllMenuGroups();
        submenu.slideDown();
        menuExpanded[styleName] = true;
    } else {
        submenu.slideUp();
        menuExpanded[styleName] = false;
    }
}

function closeAllMenuGroups() {
    $(".submenu").slideUp();
    menuExpanded = {};
}
