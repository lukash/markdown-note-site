import re
from markdown.extensions import Extension
from markdown.inlinepatterns import InlineProcessor
from markdown.util import etree

# Regular expression for detecting image style markup in format ![alt text](/image/source)[float:right;height:100px]
IMAGE_STYLE_RE = r'!\[(.*?)\]\((.*?)\)\[(.*?)\]'


class ImageStyleInlineProcessor(InlineProcessor):
    def handleMatch(self, m, data):
        # ![alt text](/image/source)[float:right;height:100px]
        alt_text = m.group(1)
        src = m.group(2)
        style = m.group(3)

        # Create an <img> tag with specified attributes
        img = etree.Element("img")
        img.set("src", src)
        img.set("alt", alt_text)
        img.set("style", style)

        return img, m.start(0), m.end(0)

class ImageStyleExtension(Extension):
    def extendMarkdown(self, md):
        # Register the custom image processor
        md.inlinePatterns.register(ImageStyleInlineProcessor(IMAGE_STYLE_RE, md), "styled_image", 175)
