'''
UML Diagram Generator Extension for Python-Markdown
===================================================

This extension adds posibility to generate UML Class and Sequence 
diagrams to Python-Markdown.
This library is using **Scruffy** to generate UML diagrams in markdown docuents
For now it is using only first line of diagram after header

Simple Usage:

    >>> import markdown
    >>> text = """
    ... Some text containg class diagram.
    ...
    ...     :::ClassDiagram(DiagramTitle)
    ...     [AbstractClass||]^[ConcreateClass||]
    ... """
    >>> print markdown.markdown(text, ['scruffy(output_dir=/tmp/)'])
    <p>Some text containg class diagram. <img alt="UML diagram" src="/images/DiagramTitle_332cf9a3e03080adc476e62f28954879924d6209.png" /></p>

Configuration option "output_dir" is required when definimg extensions to use. The slash at the end of path is mandatory.

Generated image name consists of two parts

<DiagramTitle>_<ContentHash>.png.

DiagramTitle is taken from Markdown header. It is required and should be unique for all diagrams in same directory.
ContentHash is used for determining if diagram regeneration is required. If image was changed, so as hash, it invokes regeneration of diagram. 

'''
import re
import os, glob
import hashlib
import suml.common
import suml.yuml2dot
from os.path import abspath, dirname
from markdown.extensions import Extension
from markdown.preprocessors import Preprocessor

# Global Vars
SCR_REF_RE = re.compile(r'    :::ClassDiagram.*')
SCR_LINE_RE = re.compile(r'    ')
SCR_TITLE_RE = re.compile(r'    :::ClassDiagram\(([^\)]+)\)')

class Oppp:
    """ Convert given dictionary to object"""
    def __init__(self, **entries): 
        self.__dict__.update(entries)

class Scruffy(Preprocessor):
    config = {}

    def run(self, lines):
        new_lines = []
        regexp = SCR_REF_RE
        scruffyLinesMatching = False
        scruffyLines = []

        for line in lines:
            m = regexp.match(line)
            if m and scruffyLinesMatching:
                """ If match was found and now searching for scruffy lines"""
                scruffyLines.append(line)
            elif m:
                """ If match was found, now searching for beginning line"""
                regexp = SCR_LINE_RE
                self.config['title'] = self._getTitleFromLine(line)
                scruffyLinesMatching = True
            elif scruffyLinesMatching:
                """ If match was not found, so it means scruffy lines ends"""
                imageLine = self._makeImage(scruffyLines)
                new_lines.append(imageLine)
                regexp = SCR_REF_RE
                scruffyLinesMatching = False
                del scruffyLines[:]
            else:
                new_lines.append(line)

        return new_lines

    def setConfigs(self, configs):
        for key in configs:
            self.config[key] = configs[key]

    def _makeImage(self, lines):
        path = self.config['output_dir']
        title = self.config['title']
        filename = self._generateFilename(title, lines[0])
        if (not self._isFileExists(path, filename)):
            self._removePreviousFiles(path, title) 
            print ('GENERATING....' + filename)
            fout = open(path + filename, 'wb')
            options = {
                'output': filename,
                'font': 'Purisa',
                'rankdir': 'LR',
                'png' : True,
                'svg' : False,
                'klass' : True,
                'scruffy': True,
                'shadow': True,
            }
            opt = Oppp(**options)
            suml.yuml2dot.transform(lines[0], fout, opt)

        return '![UML diagram](/images/' + filename + ')'

    def _getTitleFromLine(self, line):
            m = SCR_TITLE_RE.match(line)
            if m and m.group(1):
                return m.group(1)
            else:
                return "MissingTitle"
        

    def _generateFilename(self, title, content):
        contentHash = hashlib.sha1(str(content).encode('utf-8')).hexdigest()
        return title + '_' + contentHash + ".png"

    def _removePreviousFiles(self, path, filename):
        filelist = glob.glob(path + filename + '_*')
        for f in filelist:
             os.remove(f)

    def _isFileExists(self, path, filename):
        filelist = glob.glob(path + filename)
        if len(filelist) > 0:
             return True
        return False

class ScruffyExtension(Extension):
    configs = {
        'output_dir': '/tmp/'
    }

    def __init__(self, *args, **kwargs):
        # define default configs
        self.config = {
            'output_dir': ['/tmp/scruffy/', 'Output directory for generated images']
        }
        super(ScruffyExtension, self).__init__(*args, **kwargs)
        #for key, value in configs:
        #    self.configs[key] = value

    def extendMarkdown(self, md, md_globals):
        scruffy = Scruffy(md)
        scruffy.setConfigs(self.configs)

        # Insert instance of 'mypattern' before 'references' pattern


def makeExtension(*args, **kwargs):
    return ScruffyExtension(*args, **kwargs)

