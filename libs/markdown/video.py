import markdown
from markdown.inlinepatterns import InlineProcessor
import xml.etree.ElementTree as etree
import re

# Regular expression for detecting video URLs
VIDEO_RE = r'\!\[video\]\(([^)]+)\)'

class VideoInlineProcessor(InlineProcessor):
    def handleMatch(self, m, data):
        video_url = m.group(1)
        video_tag = etree.Element('video', width="800", controls="controls")
        source_tag = etree.SubElement(video_tag, 'source', src=video_url, type="video/webm")
        return video_tag, m.start(0), m.end(0)

class VideoExtension(markdown.extensions.Extension):
    def extendMarkdown(self, md):
        md.inlinePatterns.register(VideoInlineProcessor(VIDEO_RE, md), 'video', 175)

def makeExtension(**kwargs):
    return VideoExtension(**kwargs)
