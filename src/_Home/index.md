Markdown knowledge base
=======================
Knowledge base in Markdown has many advantages:

- markdown can be converted to any other document format (PDF, docx, HTML, ...).
- no need for any special editor
- readable in terminal
- naturally supports versioning
- indexable and searchable
- small footprint

Features:

- Written in Python
- Uses Markdown for documents, because WYSIWYG sucks
- Documents can be versioned
- Menu is generated directly from directory structure
- Design borrowed from [devdocs.io](https://devdocs.io)
- Responsive design
- Whoosh indexer
- MathJax included
- Pygments syntax highlighter
- Online Markdown editor - CodeMirror
- UML diagram generator
- Autoscroll Table of Contents
- Drag&Drop images, with automatic link creation

Markdown
========
Markdown syntax is very easy and readable.
Is supports natural formatting derived from real life experience.

Headers
-------

    Header 1
    ========

    Header 2
    ---------

Formatting
-----------
For **bold** and *italic*:

    **bold**
    *italic*

Lists
------
Bullet lists:

    - bullet 1
    - bullet 2

Numbered lists:

    1. item 1
    2. item 2

Links
-----

    [Github](https://github.com)

CBlockquote
-----------

> For block quotes, use `>` as the first character of line.

Images
-------
Images should be included from file `src/images`

    [Image discription](/images/image.png)

Code
-----
Inline code like `def main():`

    Inline code like `def main():`

For blocks of code, just indent block by 4 spaces. Optionally define language for syntax highlighter by first line `:::JavaScript`:

    :::Markdown
        :::JavaScript
        var foo = function() {}

Tables
-------

    | Column  | Column 2 | Column 3 |
    |---------|----------|----------|
    | Row 1   |          |          |
    |         | Row 2    |          |